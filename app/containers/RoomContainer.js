import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';


import MessageList from '../components/MessageList';
import MessageInput from '../components/MessageInput';
import * as UserActions from '../actions/UserActions';
import * as MessageActions from '../actions/MessageActions';

class RoomContainer extends React.Component {

  componentDidMount() {
     if(!this.props.user.curentRoom) {
      this.props.userActions.joinRoom(this.props.match.params.roomId);
    }
  }

  componentDidUpdate(prevProps) {
    if(this.props.messages.items.length && this.props.messages.items.length !== prevProps.messages.items.length) {
      this.scrollBottom();
    }
  }

  sendMessage(text) {
    const { user } = this.props;
    this.props.messageActions.sendMessage({room: user.currentRoom, text: text});
  }
  
  scrollBottom() {
    const el = document.querySelector('.overflow-container');
    el.scrollTop = el.children[0].scrollHeight;
  }

  render() {
    return (
      <div className="overflow-container" style={{overflowY: 'auto', marginBottom: '58px'}}>
        <MessageList messages={this.props.messages.items} />
        <MessageInput onSend={(text) => this.sendMessage(text)} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    messages: state.messages,
    rooms: state.rooms,
    user: state.user
  }
}

function mapDispatchToProps(dispatch) {
    return {
        userActions: bindActionCreators(UserActions, dispatch),
        messageActions: bindActionCreators(MessageActions, dispatch)
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RoomContainer));
