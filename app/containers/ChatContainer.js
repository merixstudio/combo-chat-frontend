import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

import Drawer from 'material-ui/Drawer';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import { orange500 } from 'material-ui/styles/colors';

import * as UserActions from '../actions/UserActions';
import * as RoomActions from '../actions/RoomActions';
import * as MessageActions from '../actions/MessageActions';

// import UsernameInput from '../components/UsernameInput';


class ChatContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sideNavOpen: false,
      modalOpen: false,
      username: props.user.name,
    };
  }

  componentDidMount() {
    this.props.roomActions.getRooms();
  }

  componentDidUpdate(previousProps) {
    if (previousProps.user.name !== this.props.user.name) {
      this.setState({
        username: this.props.user.name,
      });
    }
  }

  toggleNav() {
    this.setState({
      sideNavOpen: !this.state.sideNavOpen
    });
  }

  handleModalOpen() {
    this.setState({
      modalOpen: true
    });
  }

  handleModalClose() {
    this.setState({
      modalOpen: false
    });
  }

  handleRoomCreate() {
    this.props.roomActions.createRoom({ name: this.roomName.getValue() });
    this.handleModalClose();
  }

  onUsernameChange() {
    this.setState({
      username: this.userName.getValue(),
    });
  }

  handleUserNameChange() {
    this.props.userActions.changeName(this.state.username);
  }

  onRoomClick(room) {
    this.props.history.push('/room/' + room.id);
    this.props.userActions.joinRoom(room.id);

    this.setState({ sideNavOpen: false });
  }
  
  goToLobby() {
    this.props.history.push('/');
  }
  
  render() {

    const userInputStyles = {
      underlineStyle: {
        borderColor: orange500
      },
      text: {
        color: '#fff'
      }
    };

    const actions = [
      <FlatButton
        label="Submit"
        onClick={() => this.handleRoomCreate()}
        primary={true}
      />,
      <FlatButton
        label="Cancel"
        onClick={() => this.handleModalClose()}
        secondary={true}
      />
    ];

     const roomsItems = this.props.rooms.list.map((room) => {
      return (
        <ListItem primaryText={room.name} key={room.id} onClick={() => this.onRoomClick(room)}/>
      );
    });

    return (
      <MuiThemeProvider>
        <div className="container">
            <AppBar
              style={{minHeight: 64}}
              title="DevCollege React/Redux Chat Application"
              onTitleTouchTap={() => this.goToLobby()}
              iconElementRight={
                <TextField
                  name="username"
                  ref={userName => this.userName = userName}
                  value={this.state.username}
                  underlineStyle={userInputStyles.underlineStyle}
                  underlineFocusStyle={userInputStyles.underlineStyle}
                  inputStyle={userInputStyles.text}
                  onChange={() => this.onUsernameChange()}
                  onBlur={() => this.handleUserNameChange()}
                />
              }
              onLeftIconButtonTouchTap={() => this.toggleNav()}
            />
            {this.props.children}
            <Drawer open={this.state.sideNavOpen} width={400} docked={false}>
              <AppBar title="Rooms List"
                iconElementLeft={<IconButton onClick={() => this.toggleNav()}><NavigationClose /></IconButton>}
                iconElementRight={<FlatButton label="Add new room" onClick={() => this.handleModalOpen()} />} />
              <List>
              <Subheader>Rooms</Subheader>
                {(this.props.rooms.list.length > 0) ? roomsItems : <ListItem disabled={true} primaryText="No rooms" />}
              </List>
            </Drawer>
            <Dialog
              title="Name new room"
              actions={actions}
              modal={false}
              open={this.state.modalOpen}
              onRequestClose={() => this.handleModalClose()}>
              <TextField
                ref={roomName => this.roomName = roomName}
                hintText="Enter Room Name"
                floatingLabelText="Room name"
                defaultValue=""
                fullWidth={true} />
            </Dialog>
        </div>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    rooms: state.rooms
  }
}

function mapDispatchToProps(dispatch) {
    return {
        userActions: bindActionCreators(UserActions, dispatch),
        roomActions: bindActionCreators(RoomActions, dispatch),
        messageActions: bindActionCreators(MessageActions, dispatch)
    };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ChatContainer));
