import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';

import store from './store';

import * as RoomActions from './actions/RoomActions';
import * as MessageActions from './actions/MessageActions';
import * as UserActions from './actions/UserActions';

import ChatContainer from './containers/ChatContainer';
import LobbyContainer from './containers/LobbyContainer';
import RoomContainer from './containers/RoomContainer';

import wsClient from './ws-client';

wsClient.on('rooms.get', (data) => {
  store.dispatch(RoomActions.receiveRooms(data.payload.rooms));
});

wsClient.on('rooms.join', (data) => {
  store.dispatch(UserActions.roomJoined(data.payload.id));  
  store.dispatch(MessageActions.receiveMessages(data.payload.messages));
});

wsClient.on('rooms.create', (data) => {
  store.dispatch(RoomActions.addRoom(data.payload));
})

wsClient.on('messages.create', (data) => {
  store.dispatch(MessageActions.receiveMessage(data.payload));
});

wsClient.on('username.change', (data) => {
  store.dispatch(UserActions.nameChanged(data.payload.username));
})

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
      <BrowserRouter>
      <ChatContainer>
      <Route exact path="/" component={LobbyContainer} />
      <Route path="/room/:roomId" component={RoomContainer} />
      </ChatContainer>
      </BrowserRouter>
      </Provider>
    );
  }
}
