import React from 'react'; // eslint-disable-line no-unused-vars
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import App from './App';

import '../public/styles/main.scss';

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById('app')
  )
}

render(App);

if (module.hot) {
  module.hot.accept('./App', () => { render(App) });
}
