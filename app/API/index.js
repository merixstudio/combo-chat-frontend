import wsClient from '../ws-client';

class API {
    
    getRooms() {
      return wsClient.send('rooms.get');
    }    
    
    createRoom(data) {
      return wsClient.send('rooms.create', data);
    }

    joinRoom(roomId) {
      return wsClient.send('rooms.join', {room: roomId});
    }

    leaveRoom(roomId) {
      return wsClient.send('rooms.leave', {room: roomId});
    }

    createMessage(roomId, data) {
      return wsClient.send('messages.create', {room: roomId, ...data});
    }

    changeUsername(data) {
      return wsClient.send('username.change', {username: data});
    }
}

export default new API();
