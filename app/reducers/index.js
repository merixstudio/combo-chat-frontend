import { combineReducers } from 'redux';
import { user } from './user';
import { rooms } from './rooms';
import { messages } from './messages';

const rootReducer = combineReducers({
   user: user,
   rooms: rooms,
   messages: messages
});

export default rootReducer;
