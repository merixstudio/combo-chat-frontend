import { ROOM_ADD, RECEIVE_ROOMS } from '../constants/RoomActionTypes';

const initialState = {
  list: [],
};

export function rooms(state = initialState, action) {
  switch (action.type) {
    case ROOM_ADD: {
       return {
         ...state,
        list: [
          ...state.list,
          action.room
        ]
      };
    }

    case RECEIVE_ROOMS: {
      return {
        ...state,
        list: action.rooms
      };
    }

    default:
      return state;
  }
}
