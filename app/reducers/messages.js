import { RECEIVE_MESSAGE, RECEIVE_MESSAGES } from '../constants/MessageActionTypes';

const initialState = {
  items: []
};

export function messages(state = initialState, action) {
  switch (action.type) {

    case RECEIVE_MESSAGE: {
      return {
        ...state,
        items: [
          ...state.items,
          { 
            id: action.id,
            text: action.text, 
            room: action.room, 
            username: action.username, 
            created: action.created 
          }
        ]
      }
    }

    case RECEIVE_MESSAGES: {
      return {
        ...state,
        items: action.messages
      }
    }

    default:
      return state;
  }
}
