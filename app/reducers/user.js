import { USER_CHANGE_NAME, USER_JOIN_ROOM } from '../constants/UserActionTypes';

const initialState = {
  name: 'Guest',
  currentRoom: null
};

export function user(state = initialState, action) {
  switch (action.type) {
    case USER_CHANGE_NAME: {
      return {
        ...state,
        name: action.name
      };
    }
    
    case USER_JOIN_ROOM: {
      return {
        ...state,
        currentRoom: action.id
      }
    }

    default:
      return state;
  }
}
