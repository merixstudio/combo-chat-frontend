import ReconnectingWebSocket from 'shopify-reconnecting-websocket';

class WSClient {
    constructor(wsURL) {
        this.socket = new ReconnectingWebSocket(wsURL);
        this.listeners = {};
        
        this.socket.onopen = () => { };
        
        this.socket.onmessage = this.onMessage.bind(this);
    }
    
    on(eventName, callback) {
        if (this.listeners[eventName]) {
            this.listeners[eventName].push(callback);
        } else {
            this.listeners[eventName] = [callback]
        }
    }
    
    off(eventName, callback) {
        if (this.listeners[eventName]) {
            this.listeners[eventName].filter((listener) => {
                return listener !== callback;
            });
        }
    }
    
    onMessage(message) {
        const data = JSON.parse(message.data);
        const eventName = data.event;
        
        this.callListeners(eventName, data);
    }
    
    callListeners(eventName, data) {
        const listeners = this.listeners[eventName];
        
        if (listeners) {
            listeners.forEach((listener) => {
                listener.apply(listener, [data]);
            });
        }
    }
    
    send(name, data = {}) {
      if (this.socket.readyState === WebSocket.OPEN) {
        this._send(name, data);
      } else {
        setTimeout(() => this.send(name, data), 5);
      }
    }

    _send(name, data = {}) {
      this.socket.send(JSON.stringify({
        command: name,
        payload: data,
      }));
    }
}

export default new WSClient(process.env.WEBSOCKET_URL);
