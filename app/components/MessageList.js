import React from 'react';

import Message from './Message';

export default class MessageList extends React.Component {
  render() {
    const { messages } = this.props;

    const messageItems = messages.map((message) => {
      return (
        <Message text={message.text} created={message.created} username={message.username} key={message.id} />
      );
    });

    return (
      <div>
        {messageItems}
      </div>
    );
  }
}
