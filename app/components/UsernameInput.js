import React from 'react';
import TextField from 'material-ui/TextField';

export default class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    }
  }

  handleEnterKey(e) {
    if (e.nativeEvent.keyCode !== 13) {
      return;
    }
    const { onSend } = this.props;

    onSend(this.state.value);
    this.setState({
      value: ''
    });
  }

  handleChange(e) {
    this.setState({
      value: e.target.value
    });

  }

  render() {
    return (
      <div className="message-input">
        <TextField
          ref={input => this.input = input}
          hintText="Message"
          onChange={(e) => this.handleChange(e)}
          onKeyDown={(e) => this.handleEnterKey(e)}
          fullWidth={true}
          value={this.state.value} />
      </div>
    );
  }
}




export default class UsernameInput extends React.Component {


  
}
                  
                <TextField
                name="username"
                ref={userName => this.userName = userName}
                defaultValue={this.props.user.name}
                underlineStyle={userInputStyles.underlineStyle}
                underlineFocusStyle={userInputStyles.underlineStyle}
                inputStyle={userInputStyles.text}
                onBlur={() => this.handleUserNameChange()}
              />
