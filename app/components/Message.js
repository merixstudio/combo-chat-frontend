import React from 'react';

export default class Message extends React.Component {
  render() {
    const { text, created, username } = this.props;

    return (
      <div className="message">
        <p className="message-meta">
          <span className="message-from">{username}</span>
          <span className="message-time">{created}</span>
        </p>
        <p className="message-text">{text}</p>
      </div>
    );
  }
}
