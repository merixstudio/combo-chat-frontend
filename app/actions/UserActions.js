import { USER_CHANGE_NAME, USER_JOIN_ROOM } from '../constants/UserActionTypes';
import API from '../API';

export function nameChanged(name) {
  return {
    type: USER_CHANGE_NAME,
    name: name
  }
}

export function joinRoom(roomId) {
    return () => {
      return API.joinRoom(roomId);
    };
}

export function changeName(name) {
  return () => {
    return API.changeUsername(name);
  }
}

export function roomJoined(id) {
  return {
    type: USER_JOIN_ROOM,
    id: id
  }
}
