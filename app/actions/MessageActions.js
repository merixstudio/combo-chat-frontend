import { RECEIVE_MESSAGE, RECEIVE_MESSAGES } from '../constants/MessageActionTypes';
import API from '../API';

export function receiveMessage(message) {
  return {
    type: RECEIVE_MESSAGE,
    id: message.id,
    text: message.text,
    room: message.room,
    username: message.username,
    created: message.created,
  }
}

export function receiveMessages(messages) {
  return {
    type: RECEIVE_MESSAGES,
    messages: messages
  }
}

export function sendMessage(message) {
  return () => {
    return API.createMessage(message.room, {text: message.text});
  };
}
