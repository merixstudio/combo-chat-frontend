import { ROOM_ADD, RECEIVE_ROOMS } from '../constants/RoomActionTypes';
import API from '../API';


export function createRoom(room) {
  return () => {
    return API.createRoom(room);
  };
}

export function getRooms() {
  return () => {
    return API.getRooms();
  };
}

export function addRoom(room) {
  return {
    type: ROOM_ADD,
    room: room
  };
}

export function receiveRooms(rooms) {
  return {
    type: RECEIVE_ROOMS,
    rooms: rooms
  };
}
