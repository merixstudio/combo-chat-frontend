var webpack = require('webpack');
var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin'); 

module.exports = {
  entry: [
    'webpack-dev-server/client?http://0.0.0.0:8080', // WebpackDevServer host and port
    'webpack/hot/only-dev-server',
    'react-hot-loader/patch',
    './app/index.js' // Your appʼs entry point
  ],
  devtool: 'source-map',
  output: {
    path: path.join(__dirname, '/public/scripts'),
    publicPath: '/scripts/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          'babel-loader',
          'eslint-loader',
        ],
      },
      {
        test: /\.css?$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.scss?$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader'
      },
      {
        test: /\.(woff|woff2)$/,
        loader: 'url-loader',
        options: {
          prefix: 'font/',
          limit: 5000,
        },
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/octet-stream',
        },
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'image/svg+xml',
        },
      },
      {
        test: /\.gif/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'image/gif',
        },
      },
      {
        test: /\.jpg/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'image/jpg',
        },
      },
      {
        test: /\.png/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'image/png',
        },
      },
    ],
  },
  devServer: {
    contentBase: './public',
    noInfo: true, //  --no-info option
    hot: true,
    inline: true,
    historyApiFallback: true
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.EnvironmentPlugin({
      WEBSOCKET_URL: process.env.WEBSOCKET_URL || 'ws://localhost:8000/chat/',
    }),
  ]
};
